echo """
Hello this is your friendly download script!
"""

echo "Creating the directory ./embeddings/ ..."
mkdir ./embeddings

echo "About to start downloading word vector files..."

# URLS TO DOWNLOAD FROM
FASTTEXT_WIKI_300D="http://magnitude.plasticity.ai/fasttext/medium/wiki-news-300d-1M-subword.magnitude"
WORD2EVEC_GOOGLENEWS_300D="http://magnitude.plasticity.ai/word2vec/medium/GoogleNews-vectors-negative300.magnitude"
GLOVE_CCRAWL_300D="http://magnitude.plasticity.ai/glove/medium/glove.840B.300d.magnitude"

# FastTextWiki
echo "Downloading FastText 300D embeddings trained on English Wikipedia 2017..."
curl -o ./embeddings/fasttext_wiki_300d.magnitude $FASTTEXT_WIKI_300D

# DONE!
echo "All done!"
