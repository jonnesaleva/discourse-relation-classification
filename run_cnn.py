import helpers as h
import experiment as e
import preprocessing as pp
import itertools as it

"""
- Conv layers
    - {1,3,5}
    - number of filters: {50, 150, 250, 350, 450}
    - kernel size: {2,3,4}
    - padding 'valid' [ie. no padding]
    - input shape ALWAYS = (52, 300)
- 
- FC layer sizes
    - 512_128_64 + dropout
    - 512_128 + dropout
    - 512_64_128 + dropout 

- no dropout / conv dropout
"""

SMALL = True
N_EPOCHS = 10 # change this to 50 when running on AWS

DROPOUT_PROBABILITIES = [0, 0.5]
FILTER_SIZES = [50, 150, 250, 350, 450]
N_CONV_LAYERS = [1,3,5]
KERNEL_SIZES = [2,3,4]

FC_LAYER_SIZES = [[512, 128, 64], 
                  [512, 128], 
                  [512, 64, 128]]

STACKED_PADDED_FEATURES = h.load_stacked_padded_features(small=SMALL)
LABELS = h.load_labels(small=SMALL)
ORIGINAL_RELATIONS = pp.load_data(verbose=True, text_only=False)

def init_stacked_padded_experiment(
        n_conv_layers,
        filter_sizes,
        kernel_size,
        fc_layers, 
        n_epochs, 
        conv_p_dropout):
    return e.StackedPaddedConvolutionalExperiment(
            n_conv_layers=n_conv_layers,
            filter_sizes=filter_sizes, 
            kernel_size=kernel_size, 
            fc_layer_sizes=fc_layers,
            conv_p_dropout=conv_p_dropout,
            n_epochs=n_epochs,
            features=STACKED_PADDED_FEATURES, 
            labels=LABELS, 
            small=SMALL,
            original_relations=ORIGINAL_RELATIONS
    )

stacked_padded_experiments = [
    init_stacked_padded_experiment(n_conv_layers=n_conv, 
                                   filter_sizes=FILTER_SIZES, 
                                   kernel_size=k, 
                                   fc_layers=ls, 
                                   n_epochs=N_EPOCHS, 
                                   conv_p_dropout=p) 

    for n_conv, k, ls, p in it.product(N_CONV_LAYERS, 
                                       KERNEL_SIZES, 
                                       FC_LAYER_SIZES, 
                                       DROPOUT_PROBABILITIES)
]

# create experiment holders
h.create_experiment_folders(stacked_padded_experiments)

for ix, exp in enumerate(stacked_padded_experiments):
    print(f'Running exp {ix+1} / {len(stacked_padded_experiments)}')
    print(str(exp))
    exp.run()
    exp.save()
