import pickle
import pathlib

def create_experiment_folders(experiments):
    folders = [e.create_save_path() for e in experiments]
    for f in folders:
        print(f'Making the folder {f}')
        pathlib.Path(f)\
               .mkdir(parents=True, 
                      exist_ok=True) 

def load_pickle(p):
    with open(p, 'rb') as f:
        return pickle.load(f)

def dump_pickle(obj, p):
    with open(p, 'wb') as f:
        pickle.dump(obj, f)

def grab_tokens(string, n, from_end=False, padding='<PAD>'):
    """
    Grabs n tokens from beginning/end of string.
    If len(string) < n, we pad the rest with a padding token.
    """
    direction = -1 if from_end else 1
    tokens = string.split()[::direction]
    out = tokens[:n]
    if len(out) < n:
        left = n - len(out)
        out = out + left*[padding]
    return out[::direction]

def _load(file_name, small=False):
    _ = 'small/' if small else '/'
    PATH = f'./features/{_}{file_name}'
    print(f'Loading data from {PATH}')
    d = load_pickle(PATH)
    train, dev, test = (d['train'], 
                        d['dev'], 
                        d['test'])
    return train, dev, test

def load_labels(small=False):
    file_name = 'one_hot_labels.pkl'
    return _load(file_name, small)

def load_stacked_padded_features(small=False):
    file_name = 'stacked_embeddings_padded.pkl'
    return _load(file_name, small)

def load_average_padded_features(small=False):
    file_name = 'average_embeddings_padded.pkl'
    return _load(file_name, small)

def load_average_entire_features(small=False):
    file_name = 'average_embeddings_entire.pkl'
    return _load(file_name, small)

