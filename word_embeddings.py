from abc import ABC
from pymagnitude import Magnitude
import numpy as np
import helpers as h

### ABSTRACT CLASSES ### 
class WordEmbedding(ABC):
    def __init__(self, path):
        """
        Initializes word vector embedder
        """
        pass

    def transform(self, word):
        """
        Returns vector representation of `word` as numpy array
        """
        pass

class SentenceEmbedding(ABC):
    def __init__(self, *args, **kwargs):
        """
        Initializes a sentence embedder given some parameters
        """
        pass

    def transform(self, sentence):
        pass

### ACTUAL IMPLEMENTATIONS ### 

class FastTextEmbedding(WordEmbedding):
    def __init__(self, path='./embeddings/fasttext_wiki_300d.magnitude', padding='<PAD>'):
        self.path = path
        self.vectors = Magnitude(self.path)
        self.padding = padding
        self.padding_vector = np.zeros(self.vectors.dim)

    @property
    def dim(self):
        return self.vectors.dim

    def transform(self, w):
        if w == self.padding:
            return self.padding_vector
        else:
            return self.vectors.query(w)

class StackedEmbedding(SentenceEmbedding):
    """
    Stacks sentence tokens together vertically as discussed in tutorial
    """
    def __init__(self, word_embedding):
        """
        Note: must pass WordEmbedding INSTANCE, not class
        """
        self.word_embedding = word_embedding

    @property
    def dim(self):
        return self.word_embedding.dim

    def transform(self, sentence=None, tokens=None):
        if sentence is None and tokens is None:
            raise ValueError('One of sentence/tokens must be provided')
        if sentence:
            tokens = sentence.split()
       
        out = np.vstack([self.word_embedding\
                             .transform(t) for t in tokens])
        assert out.ndim == 2
        return out

class AverageEmbedding(SentenceEmbedding):
    """
    Averages sentence tokens together as in sent2vec paper

    Link to paper: https://github.com/epfml/sent2vec
    """
    def __init__(self, word_embedding, cache_path=None):
        """
        Note: must pass WordEmbedding INSTANCE, not class

        Parameters
        ----------
        word_embedding : WordEmbedding class
        cache_path : str
            path to cache of pickled vectors
        """
        self.word_embedding = word_embedding
        self.cache_path = cache_path
        if self.cache_path:
            self.cache = h.load_pickle(self.cache_path)
        else:
            self.cache = {}

    @property
    def dim(self):
        return self.word_embedding.dim

    def transform(self, sentence=None, tokens=None):
        if sentence is None and tokens is None:
            raise ValueError('One of sentence/tokens must be provided')
        if sentence:
            tokens = sentence.split()
        
        out = np.vstack([self.word_embedding\
                             .transform(t) for t in tokens])\
                .mean(axis=0)

        assert out.ndim == 1
        return out

