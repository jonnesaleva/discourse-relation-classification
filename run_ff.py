import helpers as h
import experiment as e
import preprocessing as pp
import itertools as it

SMALL = True
DROPOUT_PROBABILITIES = [0, 0.5]
N_EPOCHS = 10

FLAT_LAYER_SIZES = h.load_pickle('hyperparameters/flat_layer_conditions.pkl')

AVERAGE_ENTIRE_FEATURES = h.load_average_entire_features(small=SMALL)
AVERAGE_PADDED_FEATURES = h.load_average_padded_features(small=SMALL)
STACKED_PADDED_FEATURES = h.load_stacked_padded_features(small=SMALL)
LABELS = h.load_labels(small=SMALL)
ORIGINAL_RELATIONS = pp.load_data(verbose=True, text_only=False)

def init_average_entire_experiment(layers, n_epochs, p_dropout):
    return e.AverageEntireFeedforwardExperiment(layer_sizes=layers,
                                                p_dropout=p_dropout,
                                                n_epochs=n_epochs,
                                                features=AVERAGE_ENTIRE_FEATURES, 
                                                labels=LABELS, 
                                                small=SMALL,
                                                original_relations=ORIGINAL_RELATIONS)


def init_average_padded_experiment(layers, n_epochs, p_dropout):
    return e.AveragePaddedFeedforwardExperiment(layer_sizes=layers,
                                                p_dropout=p_dropout,
                                                n_epochs=n_epochs,
                                                features=AVERAGE_PADDED_FEATURES, 
                                                labels=LABELS, 
                                                small=SMALL,
                                                original_relations=ORIGINAL_RELATIONS)

def init_stacked_padded_experiment(layers, n_epochs, p_dropout):
    return e.StackedPaddedFeedforwardExperiment(layer_sizes=layers,
                                                p_dropout=p_dropout,
                                                n_epochs=n_epochs,
                                                features=STACKED_PADDED_FEATURES, 
                                                labels=LABELS, 
                                                small=SMALL,
                                                original_relations=ORIGINAL_RELATIONS)

average_entire_experiments = [
    init_average_entire_experiment(ls, N_EPOCHS, p) 
    for ls, p in it.product(FLAT_LAYER_SIZES, 
                            DROPOUT_PROBABILITIES)
]

average_padded_experiments = [
    init_average_padded_experiment(ls, N_EPOCHS, p) 
    for ls, p in it.product(FLAT_LAYER_SIZES, 
                            DROPOUT_PROBABILITIES)
]


stacked_padded_experiments = [
    init_stacked_padded_experiment(ls, N_EPOCHS, p) 
    for ls, p in it.product(FLAT_LAYER_SIZES, 
                            DROPOUT_PROBABILITIES)
]

experiments = average_entire_experiments + \
              average_padded_experiments + \
              stacked_padded_experiments

# create experiment holders
h.create_experiment_folders(stacked_padded_experiments)

for ix, exp in enumerate(experiments):
    print(f'Running {ix+1} / {len(experiments)}')
    print(exp)
    exp.run()
    exp.save()
