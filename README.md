# Discourse Relation Classification using Keras

# Note:

This code requires `gcc` and `g++` to be installed on your machine.

If you run into a GPU error with Tensorflow 2.0, run `conda install -c anaconda cudnn` inside your environment.

## Approach / design decisions
- We use `pymagnitude` for the word vectors so that OOV handling works nicely.
    - Since `pymagnitude` also handles subwords, we don't _need_ to perform lemmatization etc
- Number of things to pick from Arg1/Arg2 = 25, from connective = 2
    - Rationale: analysis of number of tokens (see `explore_relations.py`)

## How to run
0. Run `make install`
1. Run `./download_embeddings.sh`
2. Run `make df`
3. Optionally, run `make explore`
4. Run `make features`
5. Optionally run `make small_features`
6. Run `make test`
7. If test returns an error about `pytest`, run `pip install pytest`
8. Change `small=False` in `run_ff.py` and execute `make feedforward`
9. Change `small=False` in `run_cnn.py` and execute `make convolutional`
10. Open up the Jupyter notebook `experimental_results_analysis.ipynb`, run all the cells and enjoy. :)

