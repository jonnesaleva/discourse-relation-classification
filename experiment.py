import tensorflow as tf 
from tensorflow import keras
import preprocessing as pp
import helpers as h
import numpy as np
import json
import time
import os

class Experiment:
    """
    Generic class representing an Experiment.
    """

    def __init__(self, load_data_function, 
                       layer_sizes,
                       features=None,
                       labels=None,
                       p_dropout=0,
                       verbosity=2,
                       activation='relu', 
                       optimizer='adam', 
                       metrics=None,
                       n_epochs=10,
                       small=False,
                       original_relations=None,
                       *args, **kwargs):
        if original_relations:
            self.original_relations = original_relations
        else:
            self.original_relations = pp.load_data(text_only=False)

        self.layer_sizes = layer_sizes
        self.small = small
        self.p_dropout = p_dropout
        self.activation = activation
        self.optimizer = optimizer
        self.decoder = h.load_pickle("./features/one_hot_encoder.pkl")
        self.verbosity = verbosity
        self.loss = 'categorical_crossentropy'
        self.n_epochs = n_epochs
        self._load_data = load_data_function
        self.load_data(features=features, 
                       labels=labels, 
                       small=self.small)

        if metrics is not None:
            self.metrics = metrics
        else:
            self.metrics = ['accuracy']

        self.save_path = self.create_save_path()

    def load_data(self, features=None, labels=None, small=False):
        """
        Loads train/dev/test data and associated output labels.
        """

        if features is not None and labels is not None:
            (self.train_labels,
             self.dev_labels,
             self.test_labels) = labels

            (self.train_features,
             self.dev_features,
             self.test_features) = features
        else:
            (self.train_labels,
             self.dev_labels,
             self.test_labels) = h.load_labels(small)

            (self.train_features,
             self.dev_features,
             self.test_features) = self._load_data(small)

        (self.N_EXAMPLES, 
         self.N_CLASSES) = self.train_labels.shape

        print(f'There are {self.N_EXAMPLES} examples')
        print(f'There are {self.N_CLASSES} classes')
    
    def init_model(self):
        pass

    def fit(self):
        """
        Fits/trains the model using instance hyperparameters.
        """
        self.model.fit(self.train_features, 
                       self.train_labels,
                       epochs=self.n_epochs,
                       verbose=self.verbosity,
                       validation_data=(self.dev_features, 
                                        self.dev_labels))

    def decode(self, one_hot_labels):
        """
        Decodes one hot labels into a List of actual values.
        """
        return self.decoder\
                   .inverse_transform(one_hot_labels)\
                   .ravel()\
                   .tolist()

    def decode_to_json(self, label_predictions, kind='test'):
        """
        Decodes model prediction output to JSON format.
        Returns a list of dictionaries corresponding to JSON.
        """
        ARG1  = 'Arg1'
        ARG2  = 'Arg2'
        CONN  = 'Connective'
        SENSE = 'Sense'
        TYPE  = 'Type'
        KEYS  = [ARG1, ARG2, CONN, SENSE]
        TEXT  = 'RawText'
        TOKEN_LIST = "TokenList"
        TYPE = 'Type' 
        DOCID = 'DocID'

        rows = self.original_relations[kind]
        output = []
        for row, label_pred in zip(rows, label_predictions):
            output_row = {
                ARG1: {TOKEN_LIST: row[ARG1][TOKEN_LIST]},
                ARG2: {TOKEN_LIST: row[ARG2][TOKEN_LIST]},
                CONN: {TOKEN_LIST: row[CONN][TOKEN_LIST]},
                DOCID: row[DOCID],
                SENSE: [label_pred],
                TYPE: row[TYPE]
            }
            output.append(output_row)
        return output

    def save_json(self, output):
        """
        Saves decoded outputs to disk as JSON. Unlike pickle files,
        the JSON output can be consumed by scorer.py later on.
        """
        with open(f"{self.save_path}/experiment_output.json", 'w') as f:
            f.write("\n".join(json.dumps(x) for x in output))

    def save(self):
        """
        Saves experiment outputs + metadata to disk as pickle files.
        """
        h.dump_pickle(self.model.history.history, f"{self.save_path}/history.pkl")
        h.dump_pickle(self.model.get_config(), f"{self.save_path}/model_config.pkl")
        h.dump_pickle(self.predictions, f"{self.save_path}/predictions.pkl")
        h.dump_pickle(self.decoded_predictions, f"{self.save_path}/decoded_predictions.pkl")

    def predict_proba(self, x, *args, **kwargs):
        """
        Predicts probabilities of output labels.
        """
        return self.model.predict(x, *args, **kwargs)

    def predict(self, x, *args, **kwargs):
        """
        Predicts one-hot outputs instead of strings/integer labels.
        """
        probs = self.predict_proba(x)
        predicted_classes = np.argmax(probs, axis=1)
        onehot = np.zeros_like(probs)
        for i, c in enumerate(predicted_classes):
            onehot[i,c] = 1.0
        return onehot

    def run(self):
        """
        Initializes the model, trains it on the training data, 
        and evaluates/predicts on the test set, saving results to disk.
        """
        self.init_model()
        self.fit()
        self.model.evaluate(self.test_features, 
                                 self.test_labels)
        self.predictions = self.predict(self.test_features)
        self.decoded_predictions = self.decode(self.predictions)
        self.decoded_json = self.decode_to_json(self.decoded_predictions, 
                                                kind='test')
        self.save_json(self.decoded_json)

# Feedforward Experiments

class FeedforwardExperiment(Experiment):

    def create_save_path(self):
        """
        Utilizes hyperparameters to create a descriptive path for saving results.
        """
        layer_sizes = "_".join(str(size) for size in self.layer_sizes)
        self.folder_name = f'ff_{self.kind}__dropout{self.p_dropout}_layers_{layer_sizes}_{"small" if self.small else "large"}'
        return f'./experiments/{self.folder_name}'
    
    def init_model(self):
        """
        Initializes a feedforward neural network for an experiment.
        """

        # determine if we need to flatten
        must_flatten = bool(self.train_features.ndim == 3)

        if must_flatten:
            dim1, dim2 = self.train_features.shape[1:]
            first_layer = [keras.layers.Flatten(input_shape=(dim1, dim2))]
        else:
            first_layer = []

        hidden_layers = []
        for hidden_dim in self.layer_sizes:
            actual_layer = keras.layers.Dense(units=hidden_dim, 
                                              activation=self.activation)
            dropout_layer = keras.layers.Dropout(self.p_dropout)
            hidden_layers.append(actual_layer)
            hidden_layers.append(dropout_layer)

        softmax_layers = [
            keras.layers.Dense(units=self.N_CLASSES, 
                               activation=self.activation),
            keras.layers.Softmax()
        ]

        all_layers = first_layer + hidden_layers + softmax_layers
        self.model = keras.Sequential(all_layers)
        self.model.compile(optimizer=self.optimizer, 
                           loss=self.loss, 
                           metrics=self.metrics)

class StackedPaddedFeedforwardExperiment(FeedforwardExperiment):
    def __init__(self, layer_sizes,
                       features=None,
                       labels=None,
                       p_dropout=0,
                       verbosity=2,
                       activation='relu', 
                       optimizer='adam', 
                       metrics=None,
                       n_epochs=10,
                       small=False,
                       original_relations=None,
                       *args, **kwargs):
        super().__init__(
            load_data_function=h.load_stacked_padded_features,
            features=features,
            labels=labels,
            p_dropout=p_dropout,
            verbosity=verbosity,
            small=small,
            layer_sizes=layer_sizes,
            activation=activation,
            optimizer=optimizer,
            n_epochs=n_epochs,
            original_relations=original_relations,
            *args, **kwargs
        )

        self.kind = 'stacked_padded'

    def __repr__(self):
        return f'StackedPaddedFeedforwardExperiment(layer_sizes={self.layer_sizes}, p_dropout={self.p_dropout})'


class AveragePaddedFeedforwardExperiment(FeedforwardExperiment):
    def __init__(self, layer_sizes,
                       features=None,
                       labels=None,
                       p_dropout=0,
                       verbosity=2,
                       activation='relu', 
                       optimizer='adam', 
                       metrics=None,
                       n_epochs=10,
                       small=False,
                       original_relations=None,
                       *args, **kwargs):
        super().__init__(
            load_data_function=h.load_average_padded_features,
            features=features,
            labels=labels,
            p_dropout=p_dropout,
            verbosity=verbosity,
            small=small,
            layer_sizes=layer_sizes,
            activation=activation,
            optimizer=optimizer,
            n_epochs=n_epochs,
            original_relations=original_relations,
            *args, **kwargs
        )

        self.kind = 'average_padded'

    def __repr__(self):
        return f'AveragePaddedFeedforwardExperiment(layer_sizes={self.layer_sizes}, p_dropout={self.p_dropout})'

class AverageEntireFeedforwardExperiment(FeedforwardExperiment):
    def __init__(self, layer_sizes,
                       features=None,
                       labels=None,
                       p_dropout=0,
                       verbosity=2,
                       activation='relu', 
                       optimizer='adam', 
                       metrics=None,
                       n_epochs=10,
                       small=False,
                       *args, **kwargs):
        super().__init__(
            load_data_function=h.load_average_entire_features,
            features=features,
            labels=labels,
            p_dropout=p_dropout,
            verbosity=verbosity,
            small=small,
            layer_sizes=layer_sizes,
            activation=activation,
            optimizer=optimizer,
            n_epochs=n_epochs,
            *args, **kwargs
        )

        self.kind = 'average_entire'

    def __repr__(self):
        return f'AverageEntireFeedforwardExperiment(layer_sizes={self.layer_sizes}, p_dropout={self.p_dropout})'

# ConvolutionalExperiments

class StackedPaddedConvolutionalExperiment(Experiment):

    def __init__(self, n_conv_layers,
                       filter_sizes,
                       kernel_size,
                       fc_layer_sizes,
                       features=None,
                       labels=None,
                       padding_type='valid',
                       conv_p_dropout=0,
                       fc_p_dropout=0.5,
                       verbosity=2,
                       activation='relu', 
                       optimizer='adam', 
                       metrics=None,
                       n_epochs=10,
                       small=False,
                       original_relations=None,
                       *args, **kwargs):
    
        if original_relations:
            self.original_relations = original_relations
        else:
            self.original_relations = pp.load_data(text_only=False)

        self.n_conv_layers = n_conv_layers
        self.filter_sizes = filter_sizes[:self.n_conv_layers]
        self.kernel_size = kernel_size
        self.padding_type = padding_type
        self.fc_layer_sizes = fc_layer_sizes
        self.small = small
        self.conv_p_dropout = conv_p_dropout
        self.fc_p_dropout = fc_p_dropout
        self.activation = activation
        self.optimizer = optimizer
        self.decoder = h.load_pickle("./features/one_hot_encoder.pkl")
        self.verbosity = verbosity
        self.loss = 'categorical_crossentropy'
        self.n_epochs = n_epochs
        self.batch_size = 32
        self._load_data = h.load_stacked_padded_features
        self.load_data(features=features, 
                       labels=labels, 
                       small=self.small)

        if metrics is not None:
            self.metrics = metrics
        else:
            self.metrics = ['accuracy']

        self.kind = 'stacked_padded'
        self.save_path = self.create_save_path()

    def create_save_path(self):
        """
        Utilizes hyperparameters to create a descriptive path for saving results.
        """
        fc_layer_sizes = "_".join(str(size) for size in self.fc_layer_sizes)
        filter_sizes = "_".join(str(size) for size in self.filter_sizes)
        self.folder_name = f'conv_{self.kind}__convdropout{self.conv_p_dropout}_nconv{self.n_conv_layers}_filters{filter_sizes}_kernelsize{self.kernel_size}_fclayers_{fc_layer_sizes}_{"small" if self.small else "large"}'
        return f'./experiments/{self.folder_name}'
    
    def init_model(self):
        """
        Initializes a convolutional neural network model for an experiment.
        """

        n_words, emb_dim = self.train_features.shape[1:]
        
        # CONV LAYERS
        conv_layers = []

        for ix, n_filters in enumerate(self.filter_sizes):

            # at the first layer, we must specify input dimensions
            if ix == 0:
                actual_conv_layer = keras.layers.Conv1D(
                        n_filters, 
                        self.kernel_size, 
                        padding='valid',
                        activation=self.activation,
                        strides=1, data_format='channels_last',
                        input_shape=(n_words, emb_dim)
                    )

            # otherwise, they are inferred
            else:
                actual_conv_layer = keras.layers.Conv1D(
                        n_filters, 
                        self.kernel_size, 
                        padding='valid',
                        activation=self.activation,
                        strides=1
                    )

            conv_dropout_layer = keras.layers.Dropout(self.conv_p_dropout)
            conv_layers.append(actual_conv_layer)
            conv_layers.append(conv_dropout_layer)

        pooling_layer = keras.layers.GlobalMaxPooling1D()
        conv_layers.append(pooling_layer)

        # FULLY CONNECTED LAYERS
        fc_layers = []
        for ix, fc_dim in enumerate(self.fc_layer_sizes):
            actual_layer = keras.layers.Dense(units=fc_dim, 
                                              activation=self.activation)
            dropout_layer = keras.layers.Dropout(self.fc_p_dropout)
            fc_layers.append(actual_layer)
            fc_layers.append(dropout_layer)

        softmax_layers = [
            keras.layers.Dense(units=self.N_CLASSES, 
                               activation=self.activation),
            keras.layers.Softmax()
        ]

        all_layers = conv_layers + fc_layers + softmax_layers
        self.model = keras.Sequential(all_layers)
        self.model.compile(optimizer=self.optimizer, 
                           loss=self.loss, 
                           metrics=self.metrics)

